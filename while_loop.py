


"""
num = True

while True:
    print("Hi from while loop.")
"""
"""
number1 = 10
number2 = 20

while number1 < number2:
    print(number1)
    #number1 = number1 + 1
    number1 += 1
"""

user_input = ""

while user_input != "exit":
#while user_input not in ["exit", "quit"]:
    user_input = input("Enter any value: ")
    
    if user_input == "quit":
        pass
        #break
        #continue

    print(user_input)


"""
if ....
    ....


for ...:
    ....


while ....:
    pass

print("hello")
"""
