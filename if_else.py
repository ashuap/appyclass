

"""
a = True # 1
b = False # 0

#if not b:
if not a:
    print("Hello")
    print("Hello 1")

    if a:
        print("Hi")

"""

# True, False, ==, <=, >=, !=, and, or, not

number1 = 100
number2 = 10

# if "hello" != "hello" -> False
# if "hello" == "hello" -> True

if number1 > number2 and number1 > 1000 or number1 < 10:
    print("Hey")
elif number2 > 1:
    print("number2 > 1")
else:
    print("Hey, Hi!")

# not vs. !=

