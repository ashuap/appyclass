email_addresses = ["jbutt@gmail.com","josephine_darakjy@darakjy.org","art@venere.org","lpaprocki@hotmail.com","donette.foller@cox.net","simona@morasca.com","mitsue_tollner@yahoo.com","leota@hotmail.com","sage_wieser@cox.net","kris@gmail.com","minna_amigon@yahoo.com","amaclead@gmail.com","kiley.caldarera@aol.com","gruta@cox.net","calbares@gmail.com","mattie@aol.com","meaghan@hotmail.com","gladys.rim@rim.org","yuki_whobrey@aol.com","fletcher.flosi@yahoo.com","bette_nicka@cox.net","vinouye@aol.com","willard@hotmail.com","mroyster@royster.com","alisha@slusarski.com","allene_iturbide@cox.net","chanel.caudy@caudy.org","ezekiel@chui.com","wkusko@yahoo.com","bfigeroa@aol.com","ammie@corrio.com","francine_vocelka@vocelka.com","ernie_stenseth@aol.com","albina@glick.com","asergi@gmail.com","solange@shinko.com","jose@yahoo.com","rozella.ostrosky@ostrosky.com","valentine_gillian@gmail.com","kati.rulapaugh@hotmail.com","youlanda@aol.com","doldroyd@aol.com","roxane@hotmail.com","lperin@perin.org","erick.ferencz@aol.com","fsaylors@saylors.org","jina_briddick@briddick.com","kanisha_waycott@yahoo.com","emerson.bowley@bowley.org","bmalet@yahoo.com","bbolognia@yahoo.com","lnestle@hotmail.com","sabra@uyetake.org","mmastella@mastella.com","karl_klonowski@yahoo.com","twenner@aol.com","amber_monarrez@monarrez.org","shenika@gmail.com","delmy.ahle@hotmail.com","deeanna_juhas@gmail.com","bpugh@aol.com","jamal@vanausdal.org","cecily@hollack.org","carmelina_lindall@lindall.com","maurine_yglesias@yglesias.com","tawna@gmail.com","penney_weight@aol.com","elly_morocco@gmail.com","ilene.eroman@hotmail.com","vmondella@mondella.com","kallie.blackwood@gmail.com","johnetta_abdallah@aol.com","brhym@rhym.com","micaela_rhymes@gmail.com","tamar@hotmail.com","moon@yahoo.com","laurel_reitler@reitler.com","delisa.crupi@crupi.com","viva.toelkes@gmail.com","elza@yahoo.com","devorah@hotmail.com","timothy_mulqueen@mulqueen.org","ahoneywell@honeywell.com","dominque.dickerson@dickerson.org","lettie_isenhower@yahoo.com","mmunns@cox.net","stephaine@barfield.com","lai.gato@gato.org","stephen_emigh@hotmail.com","tshields@gmail.com","twardrip@cox.net","cory.gibes@gmail.com","danica_bruschke@gmail.com","wilda@cox.net","elvera.benimadho@cox.net","carma@cox.net","malinda.hochard@yahoo.com","natalie.fern@hotmail.com","lisha@centini.org"]

#print(email_addresses)

stats = {}

for email in email_addresses:
    user, domain = email.split("@")
    # 0 -> user, 1 -> domain

    if domain in stats:
        stats[domain] += 1
    else:
        #print(domain, "stats[domain] = 1")
        stats[domain] = 1

print(stats)
"""
print(stats)

{'gmail.com': 1, 'yahoo.com': 1}

{'gmail.com': 1, 'yahoo.com': 2}
"""
