"""
Q4. You have to write a program that will create a list and sub-list. For example:

    Enter any value: a
    Enter any value: b
    Enter any value: c
    Enter any value: save
    Enter any value: x
    Enter any value: y
    Enter any value: z
    Enter any value: save
    Enter any value: exit

    Now the output should look like -> [["a", "b", "c"], ["x", "y", "z"]]
"""

user_action =""
user_prompt = "Enter any value: "

main_list = []
sub_list = []

while user_action != "exit":
    user_action = input(user_prompt)

    if user_action != "save":
        sub_list.append(user_action)
    else:
        main_list.append(sub_list)
        sub_list = []

print(main_list)

