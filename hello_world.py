
number1 = "1000"
number2 = "10"

print(type(number1))
print("number2")
print(number2 + number1)

# 101000
# 1010
# 

"""
number3 = number2 + number1 # 101000
n = 10

# number3 -> string
# 10 -> int
print(number3 + str(n)) # 101000 + 10 => 10100010

print(type(n)) # -> int()
"""
