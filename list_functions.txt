>>> technologies2 = ["unix", "python", "windows", "cloud", "google"]
>>> print(type(technologies2))
<class 'list'>
>>> technologies2.append("aws")
>>> print(technologies2)
['unix', 'python', 'windows', 'cloud', 'google', 'aws']
>>> technologies2.index("google")
4
>>> print(technologies2[4])
google
>>> technologies2.insert(4,"linux")
>>> print(technologies2)
['unix', 'python', 'windows', 'cloud', 'linux', 'google', 'aws']
